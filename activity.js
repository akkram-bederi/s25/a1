//#2

db.fruits.aggregate([
	{$match:{"price":{$gte:50}}},
	{$count:"fruitCount"}
])

//#3

db.fruits.aggregate([
	{$match:{"stock":{$gte:20}}},
	{$count:"enoughtStock"}
])

//#4

db.fruits.aggregate([
	{$match:{"onSale":true}},
	{$group:{_id:null, avg_price:{$avg:"$price"}}}
])

//#5
db.fruits.aggregate([
	{$match:{"onSale":true}},
	{$group:{_id:null, max_price:{$max:"$price"}}}
])

//#6
db.fruits.aggregate([
	{$match:{"onSale":true}},
	{$group:{_id:null, min_price:{$min:"$price"}}}
])

